﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace CONCENTRATION.iOS
{
    [Register ("ViewController")]
    partial class ConcentrationViewController
    {
        [Outlet]
        UIKit.UIButton Button { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton card1 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton card10 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton card11 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton card12 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton card13 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton card14 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton card15 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton card16 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton card17 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton card18 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton card19 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton card2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton card20 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton card21 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton card22 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton card23 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton card24 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton card3 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton card4 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton card5 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton card6 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton card7 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton card8 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton card9 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel flipCountLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel flipCountLabelLandscape { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton startNewGameButton { get; set; }

        [Action ("startNewGame:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void startNewGame (UIKit.UIButton sender);

        [Action ("touchCard:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void touchCard (UIKit.UIButton sender);

        void ReleaseDesignerOutlets ()
        {
            if (card1 != null) {
                card1.Dispose ();
                card1 = null;
            }

            if (card10 != null) {
                card10.Dispose ();
                card10 = null;
            }

            if (card11 != null) {
                card11.Dispose ();
                card11 = null;
            }

            if (card12 != null) {
                card12.Dispose ();
                card12 = null;
            }

            if (card13 != null) {
                card13.Dispose ();
                card13 = null;
            }

            if (card14 != null) {
                card14.Dispose ();
                card14 = null;
            }

            if (card15 != null) {
                card15.Dispose ();
                card15 = null;
            }

            if (card16 != null) {
                card16.Dispose ();
                card16 = null;
            }

            if (card17 != null) {
                card17.Dispose ();
                card17 = null;
            }

            if (card18 != null) {
                card18.Dispose ();
                card18 = null;
            }

            if (card19 != null) {
                card19.Dispose ();
                card19 = null;
            }

            if (card2 != null) {
                card2.Dispose ();
                card2 = null;
            }

            if (card20 != null) {
                card20.Dispose ();
                card20 = null;
            }

            if (card21 != null) {
                card21.Dispose ();
                card21 = null;
            }

            if (card22 != null) {
                card22.Dispose ();
                card22 = null;
            }

            if (card23 != null) {
                card23.Dispose ();
                card23 = null;
            }

            if (card24 != null) {
                card24.Dispose ();
                card24 = null;
            }

            if (card3 != null) {
                card3.Dispose ();
                card3 = null;
            }

            if (card4 != null) {
                card4.Dispose ();
                card4 = null;
            }

            if (card5 != null) {
                card5.Dispose ();
                card5 = null;
            }

            if (card6 != null) {
                card6.Dispose ();
                card6 = null;
            }

            if (card7 != null) {
                card7.Dispose ();
                card7 = null;
            }

            if (card8 != null) {
                card8.Dispose ();
                card8 = null;
            }

            if (card9 != null) {
                card9.Dispose ();
                card9 = null;
            }

            if (flipCountLabel != null) {
                flipCountLabel.Dispose ();
                flipCountLabel = null;
            }

            if (flipCountLabelLandscape != null) {
                flipCountLabelLandscape.Dispose ();
                flipCountLabelLandscape = null;
            }

            if (startNewGameButton != null) {
                startNewGameButton.Dispose ();
                startNewGameButton = null;
            }
        }
    }
}