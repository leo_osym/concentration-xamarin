using Foundation;
using System;
using System.Collections.Generic;
using UIKit;

namespace CONCENTRATION.iOS
{
    public partial class UIThemeController : UIViewController, IUISplitViewControllerDelegate
    {
        private ConcentrationViewController LastSeguedToConcentrationViewController;
        private Dictionary<string, List<string>> Themes;
        private string themeName;
         
        public UIThemeController(IntPtr handle) : base(handle)
        {
        }

        public override void AwakeFromNib()
        {
            SplitViewController.Delegate = this;
        }

        [Export("splitViewController:collapseSecondaryViewController:ontoPrimaryViewController:")]
        public bool CollapseSecondViewController(UISplitViewController splitViewController, UIViewController secondaryViewController, UIViewController primaryViewController)
        {
            if((secondaryViewController as ConcentrationViewController).Theme == null) 
            {
                return true;
            }
            return false;
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            Themes = new Dictionary<String, List<String>>();
            Themes.Add("Helloween", new List<string> { "🧙‍♂️", "🙀", "🎃", "😈", "👻", "🍎", "🦇", "💀", "👽", "🧟" });
            Themes.Add("Zodiac", new List<string> { "♈️", "♉️", "♊️", "♋️", "♌️", "♍️", "♐️", "♑️", "♒️", "♏" });
            Themes.Add("Animals", new List<string> { "🐶", "🦁", "🐬", "🐙", "🦊", "🐼", "🐨", "🐭", "🦉", "🐊" });
            Themes.Add("Fruits", new List<string> { "🍏", "🍐", "🍑", "🍉", "🥝", "🍓", "🍌", "🍇", "🥭", "🍒" });
            Themes.Add("Sports", new List<string> { "⚽️", "🏀", "🏈", "⚾️", "🥎", "🎾", "🏐", "🏉", "🎱", "🥏" });
        }

        public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
        {
            base.PrepareForSegue(segue, sender);
            themeName = (sender as UIButton).CurrentTitle;
            if (segue.Identifier == "Choose Theme")
            {
                if (Themes[themeName] != null)
                {
                    var theme = Themes[themeName];
                    var cvc = (ConcentrationViewController)segue.DestinationViewController;
                    cvc.Theme = theme;
                    LastSeguedToConcentrationViewController = cvc;
                }
            }
        }

        partial void ButtonTouchUpInside(UIButton sender)
        {
            themeName = sender.CurrentTitle;
            ConcentrationViewController cvc;
            var last = SplitViewController.ViewControllers.Length - 1;
            cvc = SplitViewController.ViewControllers[last] as ConcentrationViewController;

            if (cvc != null)
            {
                cvc.Theme = Themes[themeName];
            }
            else if (LastSeguedToConcentrationViewController != null)
            {
                cvc = LastSeguedToConcentrationViewController;
                cvc.Theme = Themes[themeName];
                NavigationController.PushViewController(cvc, true);
            }
            else
            {
                PerformSegue("Choose Theme", sender);
            }
        }


    }
}
