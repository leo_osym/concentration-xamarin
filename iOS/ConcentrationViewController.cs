﻿using System;
using System.Collections.Generic;
//using System.Linq;

using UIKit;

namespace CONCENTRATION.iOS
{
    public partial class ConcentrationViewController : UIViewController
    {
        private List<UIButton> CardButtons;
        private Concentration Game;
        private List<String> EmojiChoices;
        private Dictionary<int, string> Emoji;
        private List<String> DefaultTheme; 
        private List<UIButton> VisibleCardButtons;

        private List<String> theme;
        public List<String> Theme
        {
            get { return theme; }
            set {
                theme = value;
                EmojiChoices = theme.Clone();
                UpdateEmoji();
                UpdateViewFromModel();
            }
        }



        partial void startNewGame(UIButton sender)
        {
            Game = new Concentration((VisibleCardButtons.Count + 1) / 2);
            Emoji = new Dictionary<int, string>();
            EmojiChoices = theme.Clone();
            flipCountLabel.Text = $"Flips: {Game.Flips} Scored: {Game.Points}";
            foreach(var button in VisibleCardButtons)
            {
                button.SetTitle("", UIControlState.Normal);
                button.BackgroundColor = UIColor.FromRGB(122,125,254); // blue from storyboard
            }
            // return to prev screen in portrait mode
            if(NavigationController != null)
            {
                NavigationController.PopViewController(true);
            }
        }

        partial void touchCard(UIButton button)
        {
            if (VisibleCardButtons.Contains(button)) 
            {
                var cardNumber = VisibleCardButtons.IndexOf(button);
                System.Diagnostics.Debug.WriteLine($"{cardNumber}");
                Game.ChooseCard(cardNumber);
                UpdateViewFromModel();

                flipCountLabel.Text = $"Flips: {Game.Flips} Scored: {Game.Points}";
                flipCountLabelLandscape.Text = $"Flips:\n{Game.Flips}\nScored:\n{Game.Points}";
                if (Game.Matches == (VisibleCardButtons.Count + 1) / 2) 
                {
                    flipCountLabel.Text = $"You win! Scored: {Game.Points}";
                    flipCountLabelLandscape.Text = $"You win!\nScored:\n{Game.Points}";
                }
            }
        }

        private void UpdateViewFromModel()
        {
            if (VisibleCardButtons != null)
            {
                for (int index = 0; index < VisibleCardButtons.Count; index++)
                {
                    var button = VisibleCardButtons[index];
                    var card = Game.cards[index];
                    if (card.IsFaceUp)
                    {
                        button.SetTitle(GetEmoji(card), UIControlState.Normal);
                        button.BackgroundColor = UIColor.White;

                    }
                    else
                    {
                        button.SetTitle("", UIControlState.Normal);
                        button.BackgroundColor = card.IsMatched ? UIColor.Clear : UIColor.FromRGB(122, 125, 254);
                    }
                }
            }
        }

        private void UpdateEmoji() { 
            if(Emoji != null)
            {
                for (int i = 0; i < Emoji.Count; i++)
                {
                    if(EmojiChoices.Count > 0) 
                    {
                        int randomIndex = EmojiChoices.Count.GetRandom();
                        var ST = EmojiChoices[randomIndex];
                        Emoji[i] = ST;
                        EmojiChoices.RemoveAt(randomIndex);
                    }
                }
            }
        }

        private String GetEmoji(Card card)
        {
            if (EmojiChoices == null)
                EmojiChoices = DefaultTheme.Clone();
            if (!Emoji.ContainsKey(card.Identifier) && EmojiChoices.Count > 0) 
            {
                // GetRandom extension here
                int randomIndex = EmojiChoices.Count.GetRandom();
                Emoji[card.Identifier] = EmojiChoices[randomIndex];
                EmojiChoices.RemoveAt(randomIndex);
            }
            return Emoji.ContainsKey(card.Identifier) ? Emoji[card.Identifier] : "?";
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            ButtonsInit();
            DefaultTheme = new List<string> { "🧙‍♂", "🙀", "🎃", "😈", "👻", "🍎", "🦇", "💀", "👽", "🧟" };
            ChangeButtonsSetWhileRotating();
            VisibleCardButtons = CardButtons.FindAll(c => !c.Hidden);

            Game = new Concentration((VisibleCardButtons.Count + 1) / 2);
            Emoji = new Dictionary<int, string>();
        }

        public override void ViewDidLayoutSubviews()
        {
            base.ViewDidLayoutSubviews();

            ChangeButtonsSetWhileRotating();
            VisibleCardButtons = CardButtons.FindAll(c => !c.Hidden);
            UpdateViewFromModel();
        }

        private void ButtonsInit()
        {
            CardButtons = new List<UIButton>();
            CardButtons.Add(card1);
            CardButtons.Add(card2);
            CardButtons.Add(card3);
            CardButtons.Add(card4);
            CardButtons.Add(card5);
            CardButtons.Add(card6);
            CardButtons.Add(card7);
            CardButtons.Add(card8);
            CardButtons.Add(card9);
            CardButtons.Add(card10);
            CardButtons.Add(card11);
            CardButtons.Add(card12);
            CardButtons.Add(card13);
            CardButtons.Add(card14);
            CardButtons.Add(card15);
            CardButtons.Add(card16);
            CardButtons.Add(card17);
            CardButtons.Add(card18);
            CardButtons.Add(card19);
            CardButtons.Add(card20);
            CardButtons.Add(card21);
            CardButtons.Add(card22);
            CardButtons.Add(card23);
            CardButtons.Add(card24);
        }

        // I made two button stacks hidden in Xcode Interface Builder 
        // (for different size classes, as was shown in lecture), 
        // but seems like Xamarin doesn't like me and ignores them...
        // so I hardcoded this function to be sure that buttons are really hidden:)
        //TODO: fix this in future 
        private void ChangeButtonsSetWhileRotating()
        {
            //LANDSCAPE
            if (TraitCollection.VerticalSizeClass == UIUserInterfaceSizeClass.Compact)
            {
                card13.Hidden = true;
                card14.Hidden = true;
                card15.Hidden = true;
                card16.Hidden = true;
                card17.Hidden = false;
                card18.Hidden = false;
                card19.Hidden = false;
                card20.Hidden = false;
            }
            //PORTRAIT
            else
            {
                card13.Hidden = false;
                card14.Hidden = false;
                card15.Hidden = false;
                card16.Hidden = false;
                card17.Hidden = true;
                card18.Hidden = true;
                card19.Hidden = true;
                card20.Hidden = true;
            }
        }

        public ConcentrationViewController(IntPtr handle) : base(handle)
        {
        }

    }

    public static class IntExtension
    {
        public static int GetRandom(this int upperBound)
        {
            Random random = new Random();
            int randomIndex = random.Next(0, upperBound);
            if (upperBound > 0)
            {
                return random.Next(0, upperBound);
            }
            else if (upperBound < 0)
            {
                return -random.Next(0, Math.Abs(upperBound));
            }
            else
            {
                return 0;
            }
        }
    }

    // Extension to clone existing list (not it's reference)
    public static class ListExtension
    {
        public static List<T> Clone<T>(this List<T> list) 
        {
            var newList = new List<T>();
            foreach (var item in list)
            {
                newList.Add(item);
            }
            return newList;
        }
    }
}
