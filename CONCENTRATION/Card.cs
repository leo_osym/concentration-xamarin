﻿using System;
namespace CONCENTRATION
{
    public class Card
    {

        public bool IsFaceUp = false;
        public bool IsMatched = false;
        public bool IsSeen = false;
        public int Identifier = 0;
        private static int IdentifierFactory = 0;

        public Card()
        {
            this.Identifier = GetUniqueIdentifier();

        }

        private Card(bool isFaceUp, bool isMatched, bool isSeen, int identifier, int identifierFactory) 
        {
            IsFaceUp = isFaceUp;
            IsMatched = isMatched;
            IsSeen = isSeen;
            Identifier = identifier;
            IdentifierFactory = identifierFactory;
        }

        private static int GetUniqueIdentifier()
        {
            IdentifierFactory += 1;
            return IdentifierFactory;
        }

        public static void ResetIdentifier() 
        {
            IdentifierFactory = 0;
        }

        public Card Clone()
        {
            return new Card(IsFaceUp, IsMatched, IsSeen, Identifier, IdentifierFactory);
        }
    }
}
