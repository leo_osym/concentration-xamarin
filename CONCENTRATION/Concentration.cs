﻿using System;
using System.Collections.Generic;
namespace CONCENTRATION
{
    public class Concentration
    {

        public List<Card> cards = new List<Card>();

        private int? IndexOfOneOnlyFaceUpCard;

        private int matches = 0;
        private int flips = 0;
        private int points = 0;
        private int themeNumber = 0;

        public int Matches => matches;
        public int Flips => flips;
        public int Points => points;
        public int ThemeNumber => themeNumber;

        public Concentration(int numberOfPairsOfCards)
        {

            for (int i = 0; i < numberOfPairsOfCards; i++)
            {
                var card = new Card();
                cards.Add(card);
                cards.Add(card.Clone());
            }
            cards = shuffleCards(cards);
            Card.ResetIdentifier();
        }


        public void ChooseCard(int index)
        {
            flips += 1;
            cards[index].IsFaceUp = true;
            if (!cards[index].IsMatched)
            {
                if (IndexOfOneOnlyFaceUpCard != null && IndexOfOneOnlyFaceUpCard != index)
                {
                    int matchIndex = IndexOfOneOnlyFaceUpCard.Value;
                    if (cards[matchIndex].Identifier == cards[index].Identifier)
                    {
                        cards[matchIndex].IsMatched = true;
                        cards[index].IsMatched = true;
                        //if matched - set points
                        matches += 1;
                        points += 2;
                    }
                    else
                    {
                        //if not matched - set penalty
                        if (Matches > 0 & (cards[matchIndex].IsSeen | cards[index].IsSeen))
                        {
                            points -= 1;
                        }

                        cards[matchIndex].IsSeen = true;
                        cards[index].IsSeen = true;
                    }

                    IndexOfOneOnlyFaceUpCard = null;
                }
                else
                {
                    for (int flipDownIndex = 0; flipDownIndex < cards.Count; flipDownIndex++)
                    {
                        cards[flipDownIndex].IsFaceUp = false;
                    }
                    cards[index].IsFaceUp = true;
                    IndexOfOneOnlyFaceUpCard = index;
                }
            }
        }

        private List<Card> shuffleCards(List<Card> unShuffledCards)
        {
            // make copy of card's array
            var shuffledCards = new List<Card>();
            Random random = new Random();
            int randomIndex = 0;
            Card temp;
            foreach (Card card in unShuffledCards)
            {
                shuffledCards.Add(card);
            }
            // shuffle cards
            for (int i = 0; i < shuffledCards.Count; i++)
            {
                randomIndex = random.Next(0, shuffledCards.Count - 1);
                temp = shuffledCards[i];
                shuffledCards[i] = shuffledCards[randomIndex];
                shuffledCards[randomIndex] = temp;
            }

            return shuffledCards;
        }
    }
}
